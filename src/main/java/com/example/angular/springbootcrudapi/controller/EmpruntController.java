package com.example.angular.springbootcrudapi.controller;

import com.example.angular.springbootcrudapi.ResourceNotFoundException;
import com.example.angular.springbootcrudapi.model.Emprunt;
import com.example.angular.springbootcrudapi.repository.EmpruntRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api")
public class EmpruntController {

    @Autowired
    private EmpruntRepository empruntRepository;

    @PostMapping("/emprunts")
    public Emprunt addEmprunt(@RequestBody Emprunt emprunt) {

        return empruntRepository.save(emprunt);
    }


    @GetMapping("/emprunts")
    public ResponseEntity<List<Emprunt>> getAllEmprunts() {
        return ResponseEntity.ok(empruntRepository.findAll());
    }

    @GetMapping("/emprunts/{id}")
    public ResponseEntity<Emprunt> getEmpruntById(@PathVariable(value = "id") Integer empruntId)
            throws ResourceNotFoundException {
        Emprunt emprunt = empruntRepository.findById(empruntId)
                .orElseThrow(() -> new ResourceNotFoundException("Emprunt not found for this id :: " + empruntId));
        return ResponseEntity.ok().body(emprunt);
    }

    @GetMapping("/empruntsUsagerOeuvre/{nom}/{oeuvreTitre}")
    public ResponseEntity<List<Emprunt>> getEmpruntByUsagerNameAndOeuvreTitre (@PathVariable(value = "nom") String usagerNom, @PathVariable(value = "oeuvreTitre") String oeuvreTitre) {
        return ResponseEntity.ok(empruntRepository.getEmpruntByUsagerNameAndOeuvreTitre("%"+usagerNom+"%", "%"+oeuvreTitre+"%"));
    }

    @GetMapping("/empruntsUsager/{nom}")
    public ResponseEntity<List<Emprunt>> getEmpruntByUsagerName (@PathVariable(value = "nom") String usagerNom) {
        return ResponseEntity.ok(empruntRepository.getEmpruntByUsagerName("%"+usagerNom+"%"));
    }

    @GetMapping("/empruntsOeuvre/{oeuvreTitre}")
    public ResponseEntity<List<Emprunt>> getEmpruntByOeuvreTitre (@PathVariable(value = "oeuvreTitre") String oeuvreTitre) {
        return ResponseEntity.ok(empruntRepository.getEmpruntByOeuvreTitre("%"+oeuvreTitre+"%"));
    }

    @GetMapping("/empruntsUsagerId/{id}")
    public ResponseEntity<Emprunt> getEmpruntByUsagerId (@PathVariable(value = "id") int usagerId)
            throws ResourceNotFoundException {
        Emprunt emprunt = empruntRepository.getEmpruntByUsagerId(usagerId);
        return ResponseEntity.ok().body(emprunt);
    }

    @GetMapping("/empruntsOeuvreId/{id}")
    public ResponseEntity<Emprunt> getEmpruntByOeuvreId (@PathVariable(value = "id") int oeuvreId)
            throws ResourceNotFoundException {
        Emprunt emprunt = empruntRepository.getEmpruntByOeuvreId(oeuvreId);
        return ResponseEntity.ok().body(emprunt);
    }

    @DeleteMapping("/emprunts/{id}")
    public Map<String, Boolean> deleteEmprunt(@PathVariable(value = "id") Integer empruntId)
            throws ResourceNotFoundException {
        Emprunt emprunt = empruntRepository.findById(empruntId)
                .orElseThrow(() -> new ResourceNotFoundException("Emprunt not found for this id :: " + empruntId));

        empruntRepository.delete(emprunt);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }
}


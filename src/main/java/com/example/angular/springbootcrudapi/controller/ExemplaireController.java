package com.example.angular.springbootcrudapi.controller;

import com.example.angular.springbootcrudapi.ResourceNotFoundException;
import com.example.angular.springbootcrudapi.model.Emprunt;
import com.example.angular.springbootcrudapi.model.Exemplaire;
import com.example.angular.springbootcrudapi.model.Oeuvre;
import com.example.angular.springbootcrudapi.repository.ExemplaireRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Query;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api")
public class ExemplaireController {

    @Autowired
    private ExemplaireRepository exemplaireRepository;

    @PostMapping("/exemplaires")
    public Exemplaire addExemplaire(@RequestBody Exemplaire exemplaire) {
        return exemplaireRepository.save(exemplaire);
    }

    @GetMapping("/exemplaires")
    public ResponseEntity<List<Exemplaire>> getAllExemplaires() {

        return ResponseEntity.ok(exemplaireRepository.findAll());
    }

    @GetMapping("/exemplairesOeuvreDispo/{id}")
    public ResponseEntity<List<Exemplaire>> getExemplairesOfOeuvreDispo(@PathVariable(value = "id") Integer oeuvreId)
            throws ResourceNotFoundException {
        return ResponseEntity.ok(exemplaireRepository.findByOeuvreIdDispo(oeuvreId));
    }

    @GetMapping("/exemplairesOeuvreId/{id}")
    public ResponseEntity<Exemplaire> getExemplaireByOeuvreId (@PathVariable(value = "id") int oeuvreId)
            throws ResourceNotFoundException {
        Exemplaire exemplaire = exemplaireRepository.getExemplaireByOeuvreId(oeuvreId);
        return ResponseEntity.ok().body(exemplaire);
    }

    @GetMapping("/exemplairesOeuvre/{id}")
    public ResponseEntity<List<Exemplaire>> getExemplairesOfOeuvre(@PathVariable(value = "id") Integer oeuvreId)
            throws ResourceNotFoundException {
        return ResponseEntity.ok(exemplaireRepository.findByOeuvreId(oeuvreId));
    }

    @GetMapping("/exemplairesEmprunt/{id}")
    public ResponseEntity<Exemplaire> getExemplairesEmprunt(@PathVariable(value = "id") Integer exemplaireId)
            throws ResourceNotFoundException {
        return ResponseEntity.ok(exemplaireRepository.getOneExemplaireEmprunt(exemplaireId));
    }

    @GetMapping("/exemplaires/{id}")
    public ResponseEntity<Exemplaire> getExemplaireById(@PathVariable(value = "id") Integer exemplaireId)
            throws ResourceNotFoundException {
        Exemplaire exemplaire = exemplaireRepository.findById(exemplaireId)
                .orElseThrow(() -> new ResourceNotFoundException("Exemplaire not found for this id :: " + exemplaireId));
        return ResponseEntity.ok().body(exemplaire);
    }

    @PutMapping("/exemplaires/{id}")
    public ResponseEntity<Exemplaire> updateExemplaire(@PathVariable(value = "id") Integer exemplaireId,
                                               @RequestBody Exemplaire exemplaireDetails) throws ResourceNotFoundException {
        System.out.println("CuxgcozrzjugcozvgFBOU ZEGFQEGOF ZEQ FGZEUO FGZR GFUZ");

        Exemplaire exemplaire = exemplaireRepository.findById(exemplaireId)
                .orElseThrow(() -> new ResourceNotFoundException("Exemplaire not found for this id :: " + exemplaireId));

        System.out.println(exemplaireDetails.getEtat());
        exemplaire.setEtat(exemplaireDetails.getEtat());
        final Exemplaire updatedExemplaire = exemplaireRepository.save(exemplaire);
        return ResponseEntity.ok(updatedExemplaire);
    }

    @DeleteMapping("/exemplaires/{id}")
    public Map<String, Boolean> deleteExemplaire(@PathVariable(value = "id") Integer exemplaireId)
            throws ResourceNotFoundException {
        Exemplaire exemplaire = exemplaireRepository.findById(exemplaireId)
                .orElseThrow(() -> new ResourceNotFoundException("Exemplaire not found for this id :: " + exemplaireId));

        exemplaireRepository.delete(exemplaire);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }
}


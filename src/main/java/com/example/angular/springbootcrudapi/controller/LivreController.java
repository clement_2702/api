package com.example.angular.springbootcrudapi.controller;

import com.example.angular.springbootcrudapi.ResourceNotFoundException;
import com.example.angular.springbootcrudapi.model.Livre;
import com.example.angular.springbootcrudapi.model.Livre;
import com.example.angular.springbootcrudapi.model.Oeuvre;
import com.example.angular.springbootcrudapi.repository.LivreRepository;

import com.example.angular.springbootcrudapi.repository.OeuvreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api")
public class LivreController {

    @Autowired
    private OeuvreRepository oeuvreRepository;
    @Autowired
    private LivreRepository livreRepository;

    @PostMapping("/livresOeuvre/{titre}")
    public Livre addLivre(@RequestBody Livre livre, @PathVariable(value = "titre") String titre) {
        Oeuvre oeuvre=new Oeuvre();
        oeuvre.setTitre(titre);
        oeuvre = oeuvreRepository.save(oeuvre);
        livre.setOeuvreId(oeuvre.getId());
        return livreRepository.save(livre);
    }

    @PostMapping("/livres")
    public Livre addLivre(@RequestBody Livre livre) {
        return livreRepository.save(livre);
    }


    @GetMapping("/livres")
    public ResponseEntity<List<Livre>> getAllLivres() {
        return ResponseEntity.ok(livreRepository.findAll());
    }

    @GetMapping("/livres/{id}")
    public ResponseEntity<Livre> getLivreById(@PathVariable(value = "id") Integer livreId)
            throws ResourceNotFoundException {
        Livre livre = livreRepository.findById(livreId)
                .orElseThrow(() -> new ResourceNotFoundException("Livre not found for this id :: " + livreId));
        return ResponseEntity.ok().body(livre);
    }

    @GetMapping("/livresOeuvreId/{id}")
    public ResponseEntity<Livre> getLivreByOeuvreId(@PathVariable(value = "id") Integer oeuvreId)
            throws ResourceNotFoundException {
        Livre livre = livreRepository.getLivreByOeuvreId(oeuvreId);
        return ResponseEntity.ok().body(livre);
    }

    @PutMapping("/livres/{id}")
    public ResponseEntity<Livre> updateLivre(@PathVariable(value = "id") Integer livreId,
                                               @RequestBody Livre livreDetails) throws ResourceNotFoundException {
        Livre livre = livreRepository.findById(livreId)
                .orElseThrow(() -> new ResourceNotFoundException("Livre not found for this id :: " + livreId));

        livre.setResume(livreDetails.getResume());

        final Livre updatedLivre = livreRepository.save(livre);
        return ResponseEntity.ok(updatedLivre);
    }

    @PutMapping("/livresOeuvre/{titre}")
    public ResponseEntity<Livre> updateLivre(@PathVariable(value = "titre") String titre,
                                                   @RequestBody Livre livreDetails) throws ResourceNotFoundException {
        Livre livre = livreRepository.findById(livreDetails.getId())
                .orElseThrow(() -> new ResourceNotFoundException("Livre not found for this id :: " + titre));

        Oeuvre oeuvre = oeuvreRepository.findById(livreDetails.getOeuvreId())
                .orElseThrow(() -> new ResourceNotFoundException("Livre not found for this id :: " + titre));

        livre.setResume(livreDetails.getResume());
        livre.setAuteur(livreDetails.getAuteur());
        livre.setDate_edition(livreDetails.getDate_edition());

        oeuvre.setTitre(titre);
        oeuvre = oeuvreRepository.save(oeuvre);
        final Livre updatedLivre = livreRepository.save(livre);
        return ResponseEntity.ok(updatedLivre);
    }

    @DeleteMapping("/livresOeuvre/{id}")
    public Map<String, Boolean> deleteLivreOeuvre(@PathVariable(value = "id") Integer oeuvreId)
            throws ResourceNotFoundException {
        Livre livre = livreRepository.findByOeuvreId(oeuvreId)
                .orElseThrow(() -> new ResourceNotFoundException("Livre not found for this id :: " + oeuvreId));
        livreRepository.delete(livre);
        Oeuvre oeuvre = oeuvreRepository.findById(oeuvreId)
                    .orElseThrow(() -> new ResourceNotFoundException("Livre not found for this id :: " + oeuvreId));
        oeuvreRepository.delete(oeuvre);

        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }

    @DeleteMapping("/livres/{id}")
    public Map<String, Boolean> deleteLivre(@PathVariable(value = "id") Integer livreId)
            throws ResourceNotFoundException {
        Livre livre = livreRepository.findById(livreId)
                .orElseThrow(() -> new ResourceNotFoundException("Livre not found for this id :: " + livreId));

        livreRepository.delete(livre);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }
}


package com.example.angular.springbootcrudapi.controller;

import com.example.angular.springbootcrudapi.ResourceNotFoundException;
import com.example.angular.springbootcrudapi.model.Magazine;
import com.example.angular.springbootcrudapi.model.Magazine;
import com.example.angular.springbootcrudapi.model.Oeuvre;
import com.example.angular.springbootcrudapi.repository.MagazineRepository;

import com.example.angular.springbootcrudapi.repository.OeuvreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api")
public class MagazineController {

    @Autowired
    private OeuvreRepository oeuvreRepository;
    @Autowired
    private MagazineRepository magazineRepository;

    @PostMapping("/magazinesOeuvre/{titre}")
    public Magazine addMagazine(@RequestBody Magazine magazine, @PathVariable(value = "titre") String titre) {
        Oeuvre oeuvre=new Oeuvre();
        oeuvre.setTitre(titre+" "+magazine.getNumero());
        oeuvre = oeuvreRepository.save(oeuvre);
        magazine.setOeuvreId(oeuvre.getId());
        return magazineRepository.save(magazine);
    }

    @PostMapping("/magazines")
    public Magazine addMagazine(@RequestBody Magazine magazine) {
        return magazineRepository.save(magazine);
    }
//

    @GetMapping("/magazines")
    public ResponseEntity<List<Magazine>> getAllMagazines() {

        return ResponseEntity.ok(magazineRepository.findAll());
    }

    @GetMapping("/magazines/{id}")
    public ResponseEntity<Magazine> getMagazineById(@PathVariable(value = "id") Integer magazineId)
            throws ResourceNotFoundException {
        Magazine magazine = magazineRepository.findById(magazineId)
                .orElseThrow(() -> new ResourceNotFoundException("Magazine not found for this id :: " + magazineId));
        return ResponseEntity.ok().body(magazine);
    }

    @GetMapping("/magazinesOeuvreId/{id}")
    public ResponseEntity<Magazine> getMagazineByOeuvreId(@PathVariable(value = "id") Integer oeuvreId)
            throws ResourceNotFoundException {
        Magazine magazine = magazineRepository.getMagazineByOeuvreId(oeuvreId);
        return ResponseEntity.ok().body(magazine);
    }

    @DeleteMapping("/magazinesOeuvre/{id}")
    public Map<String, Boolean> deleteMagazineOeuvre(@PathVariable(value = "id") Integer oeuvreId)
            throws ResourceNotFoundException {
        Magazine magazine = magazineRepository.findByOeuvreId(oeuvreId)
                .orElseThrow(() -> new ResourceNotFoundException("Magazine not found for this id :: " + oeuvreId));
        magazineRepository.delete(magazine);
        Oeuvre oeuvre = oeuvreRepository.findById(oeuvreId)
                .orElseThrow(() -> new ResourceNotFoundException("Magazine not found for this id :: " + oeuvreId));
        oeuvreRepository.delete(oeuvre);

        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }

    @PutMapping("/magazinesOeuvre/{titre}")
    public ResponseEntity<Magazine> updateMagazine(@PathVariable(value = "titre") String titre,
                                             @RequestBody Magazine magazineDetails) throws ResourceNotFoundException {
        Magazine magazine = magazineRepository.findById(magazineDetails.getId())
                .orElseThrow(() -> new ResourceNotFoundException("Magazine not found for this id :: " + titre));

        Oeuvre oeuvre = oeuvreRepository.findById(magazineDetails.getOeuvreId())
                .orElseThrow(() -> new ResourceNotFoundException("Magazine not found for this id :: " + titre));

        magazine.setNumero(magazineDetails.getNumero());
        magazine.setPeriodicite(magazineDetails.getPeriodicite());
        magazine.setDate_parution(magazineDetails.getDate_parution());

        oeuvre.setTitre(titre);
        oeuvre = oeuvreRepository.save(oeuvre);

        oeuvreRepository.save(oeuvre);
        final Magazine updatedMagazine = magazineRepository.save(magazine);
        return ResponseEntity.ok(updatedMagazine);
    }

    @DeleteMapping("/magazines/{id}")
    public Map<String, Boolean> deleteMagazine(@PathVariable(value = "id") Integer magazineId)
            throws ResourceNotFoundException {
        Magazine magazine = magazineRepository.findById(magazineId)
                .orElseThrow(() -> new ResourceNotFoundException("Magazine not found for this id :: " + magazineId));

        magazineRepository.delete(magazine);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }
}


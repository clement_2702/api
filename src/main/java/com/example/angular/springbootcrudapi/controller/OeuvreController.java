package com.example.angular.springbootcrudapi.controller;

import com.example.angular.springbootcrudapi.ResourceNotFoundException;
import com.example.angular.springbootcrudapi.model.Livre;
import com.example.angular.springbootcrudapi.model.Magazine;
import com.example.angular.springbootcrudapi.model.Oeuvre;
import com.example.angular.springbootcrudapi.model.Reservation;
import com.example.angular.springbootcrudapi.repository.LivreRepository;
import com.example.angular.springbootcrudapi.repository.MagazineRepository;
import com.example.angular.springbootcrudapi.repository.OeuvreRepository;

import com.example.angular.springbootcrudapi.repository.ReservationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api")
public class OeuvreController {

    @Autowired
    private LivreRepository livreRepository;
    @Autowired
    private MagazineRepository magazineRepository;
    @Autowired
    private OeuvreRepository oeuvreRepository;
    @Autowired
    private ReservationRepository reservationRepository;

    @PostMapping("/oeuvres")
    public Oeuvre addOeuvre(@RequestBody Oeuvre oeuvre) {
        return oeuvreRepository.save(oeuvre);
    }


    @GetMapping("/oeuvres")
    public ResponseEntity<List<Oeuvre>> getAllOeuvres() {

        return ResponseEntity.ok(oeuvreRepository.findAll());
    }

    @GetMapping("/oeuvres/{id}")
    public ResponseEntity<Oeuvre> getOeuvreById(@PathVariable(value = "id") Integer oeuvreId)
            throws ResourceNotFoundException {
        Oeuvre oeuvre = oeuvreRepository.findById(oeuvreId)
                .orElseThrow(() -> new ResourceNotFoundException("Oeuvre not found for this id :: " + oeuvreId));
        return ResponseEntity.ok().body(oeuvre);
    }

    @GetMapping("/oeuvresExemplaireId/{exemplaireId}")
    public ResponseEntity<Oeuvre> getOeuvreByExemplaireId(@PathVariable(value = "exemplaireId") Integer exemplaireId)
            throws ResourceNotFoundException {
        Oeuvre oeuvre = oeuvreRepository.getOeuvreByExemplaireId(exemplaireId);
        return ResponseEntity.ok().body(oeuvre);
    }

    @PutMapping("/oeuvres/subReservation/{id}")
    public ResponseEntity<Oeuvre> updateOeuvreSubReservation(@PathVariable(value = "id") Integer oeuvreId) throws ResourceNotFoundException {
        Oeuvre oeuvre = oeuvreRepository.findById(oeuvreId)
                .orElseThrow(() -> new ResourceNotFoundException("Oeuvre not found for this id :: " + oeuvreId));
        final Oeuvre updatedOeuvre = oeuvreRepository.save(oeuvre);
        return ResponseEntity.ok(updatedOeuvre);
    }

    @PutMapping("/oeuvres/{id}")
    public ResponseEntity<Oeuvre> updateOeuvre(@PathVariable(value = "id") Integer oeuvreId,
                                               @RequestBody Oeuvre oeuvreDetails) throws ResourceNotFoundException {
        Oeuvre oeuvre = oeuvreRepository.findById(oeuvreId)
                .orElseThrow(() -> new ResourceNotFoundException("Oeuvre not found for this id :: " + oeuvreId));

        oeuvre.setTitre(oeuvre.getTitre());

        final Oeuvre updatedOeuvre = oeuvreRepository.save(oeuvre);
        return ResponseEntity.ok(updatedOeuvre);
    }

    @DeleteMapping("/oeuvresMagLivre/{id}")
    public Map<String, Boolean> deleteOeuvreMagLivre(@PathVariable(value = "id") Integer oeuvreId) {
        try {
            Livre livre = livreRepository.findByOeuvreId(oeuvreId)
                    .orElseThrow(() -> new ResourceNotFoundException("Oeuvre not found for this id :: " + oeuvreId));;
            livreRepository.delete(livre);
        } catch (Exception e) { }

        try {
            Magazine magazine = magazineRepository.findByOeuvreId(oeuvreId)
                    .orElseThrow(() -> new ResourceNotFoundException("Oeuvre not found for this id :: " + oeuvreId));;
            magazineRepository.delete(magazine);
        } catch (Exception e) { }

        Oeuvre oeuvre = oeuvreRepository.findById(oeuvreId)
                .orElseThrow(() -> new ResourceNotFoundException("Oeuvre not found for this id :: " + oeuvreId));
        oeuvreRepository.delete(oeuvre);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }

    @DeleteMapping("/oeuvres/{id}")
    public Map<String, Boolean> deleteOeuvre(@PathVariable(value = "id") Integer oeuvreId)
            throws ResourceNotFoundException {
        // Delete les reservtaion avec l'oeuvre
        List<Reservation> reservations = reservationRepository.findByOeuvreId(oeuvreId);

        for (Reservation reservation : reservations) {
            reservationRepository.delete(reservation);
        }

        // Delete de l'oeuvre
        Oeuvre oeuvre = oeuvreRepository.findById(oeuvreId)
                .orElseThrow(() -> new ResourceNotFoundException("Oeuvre not found for this id :: " + oeuvreId));

        oeuvreRepository.delete(oeuvre);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }
}


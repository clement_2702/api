package com.example.angular.springbootcrudapi.controller;

import com.example.angular.springbootcrudapi.ResourceNotFoundException;
import com.example.angular.springbootcrudapi.model.Emprunt;
import com.example.angular.springbootcrudapi.model.Reservation;
import com.example.angular.springbootcrudapi.repository.EmpruntRepository;
import com.example.angular.springbootcrudapi.repository.ReservationRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api")
public class ReservationController {
	@Autowired
	private ReservationRepository reservationRepository;

	@PostMapping("/reservations")
	public Reservation addReservation(@RequestBody Reservation reservation) {
		return reservationRepository.save(reservation);
	}

	@GetMapping("/reservations")
	public ResponseEntity<List<Reservation>> getAllReservations() {
		return ResponseEntity.ok(reservationRepository.findAll());
	}

	@GetMapping("/reservations/{id}")
	public ResponseEntity<Reservation> getReservationById(@PathVariable(value = "id") Integer reservationId)
			throws ResourceNotFoundException {
		Reservation reservation = reservationRepository.findById(reservationId)
				.orElseThrow(() -> new ResourceNotFoundException("Reservation not found for this id :: " + reservationId));
		return ResponseEntity.ok().body(reservation);
	}

	@DeleteMapping("/reservations/{id}")
	public Map<String, Boolean> deleteReservation(@PathVariable(value = "id") Integer reservationId)
			throws ResourceNotFoundException {
		Reservation reservation = reservationRepository.findById(reservationId)
				.orElseThrow(() -> new ResourceNotFoundException("Reservation not found for this id :: " + reservationId));

		reservationRepository.delete(reservation);
		Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", Boolean.TRUE);
		return response;
	}

	@DeleteMapping("/reservationsOeuvreAndUsager/{oeuvreId}/{usagerId}")
	public Map<String, Boolean> deleteReservationOeuvreAndUsager(@PathVariable(value = "oeuvreId") Integer oeuvreId,
																 @PathVariable(value = "usagerId") Integer usagerId)
			throws ResourceNotFoundException {
		List<Reservation> reservations = reservationRepository.findByUsagerIdAndOeuvreId(usagerId, oeuvreId);

		for (Reservation reservation : reservations) {
			reservation.getId();
			reservationRepository.delete(reservation);
		}

		Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", Boolean.TRUE);
		return response;
	}
}

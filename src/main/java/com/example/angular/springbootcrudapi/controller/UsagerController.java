package com.example.angular.springbootcrudapi.controller;

import com.example.angular.springbootcrudapi.ResourceNotFoundException;
import com.example.angular.springbootcrudapi.model.Reservation;
import com.example.angular.springbootcrudapi.model.Usager;
import com.example.angular.springbootcrudapi.repository.ReservationRepository;
import com.example.angular.springbootcrudapi.repository.UsagerRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api")
public class UsagerController {

    @Autowired
    private UsagerRepository usagerRepository;
    @Autowired
    private ReservationRepository reservationRepository;

    @PostMapping("/usagers")
    public Usager addUsager(@RequestBody Usager usager) {
        return usagerRepository.save(usager);
    }


    @GetMapping("/usagers")
    public ResponseEntity<List<Usager>> getAllUsagers() {

        return ResponseEntity.ok(usagerRepository.findAll());
    }

    @GetMapping("/usagers/{id}")
    public ResponseEntity<Usager> getUsagerById(@PathVariable(value = "id") Integer usagerId)
            throws ResourceNotFoundException {
        Usager usager = usagerRepository.findById(usagerId)
                .orElseThrow(() -> new ResourceNotFoundException("Usager not found for this id :: " + usagerId));
        return ResponseEntity.ok().body(usager);
    }

    @PutMapping("/usagers/{id}")
    public ResponseEntity<Usager> updateUsager(@PathVariable(value = "id") Integer usagerId,
                                               @RequestBody Usager usagerDetails) throws ResourceNotFoundException {
        Usager usager = usagerRepository.findById(usagerId)
                .orElseThrow(() -> new ResourceNotFoundException("Usager not found for this id :: " + usagerId));

        usager.setNom(usagerDetails.getNom());
        usager.setPrenom(usagerDetails.getPrenom());
        usager.setAdresse(usagerDetails.getAdresse());
        usager.setTelephone(usagerDetails.getTelephone());

        final Usager updatedUsager = usagerRepository.save(usager);
        return ResponseEntity.ok(updatedUsager);
    }

    @DeleteMapping("/usagers/{id}")
    public Map<String, Boolean> deleteUsager(@PathVariable(value = "id") Integer usagerId)
            throws RuntimeException {

        // Delete usager
        Usager usager = usagerRepository.findById(usagerId)
                .orElseThrow(() -> new RuntimeException("Usager not found for this id :: " + usagerId));
        usagerRepository.delete(usager);

        // Delete les reservations de l'usager
        List<Reservation> reservations = reservationRepository.findByUsagerId(usagerId);
        for (Reservation reservation : reservations) {
            reservationRepository.delete(reservation);
        }

        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }
}


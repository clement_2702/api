package com.example.angular.springbootcrudapi.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Time;
import java.util.Calendar;


@NoArgsConstructor
@AllArgsConstructor
@Table
@Entity
@SequenceGenerator(name="seq", initialValue=1, allocationSize=1)
public class Emprunt {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private int exemplaire_id;
    private int usager_id;
    private int actif=1;
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq")
    private java.sql.Date date=new java.sql.Date(Calendar.getInstance().getTime().getTime());



    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public int getExemplaire_id() {
        return exemplaire_id;
    }
    public void setExemplaire_id(int exemplaire_id) {
        this.exemplaire_id = exemplaire_id;
    }
    public int getUsager_id() {
        return usager_id;
    }
    public void setUsager_id(int usager_id) {
        this.usager_id = usager_id;
    }
    public int getActif() {
        return actif;
    }
    public void setActif(int actif) {
        this.actif = actif;
    }
    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}

package com.example.angular.springbootcrudapi.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;


@NoArgsConstructor
@AllArgsConstructor
@Table
@Entity
@SequenceGenerator(name="seq", initialValue=0, allocationSize=1)
public class Exemplaire {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private int oeuvreId;
    private String etat;

    public String getEtat() { return etat; }
    public void setEtat(String etat) { this.etat = etat; }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public int getOeuvreId() { return oeuvreId; }
    public void setOeuvreId(int oeuvreId) { this.oeuvreId = oeuvreId; }
}

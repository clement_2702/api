package com.example.angular.springbootcrudapi.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Time;
import java.util.Calendar;


@NoArgsConstructor
@AllArgsConstructor
@Table
@Entity
@SequenceGenerator(name="seq", initialValue=1, allocationSize=1)
public class Livre {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String resume;
    private int oeuvreId;
    private String auteur;
    private java.sql.Date date_edition;



    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getAuteur() {
        return auteur;
    }
    public void setAuteur(String auteur) {
        this.auteur = auteur;
    }
    public String getResume() {
        return resume;
    }
    public void setResume(String resume) {
        this.resume = resume;
    }
    public int getOeuvreId() {
        return oeuvreId;
    }
    public void setOeuvreId(int oeuvre_id) {
        this.oeuvreId = oeuvre_id;
    }
    public Date getDate_edition() {
        return date_edition;
    }
    public void setDate_edition(Date date_edition) {
        this.date_edition = date_edition;
    }
}

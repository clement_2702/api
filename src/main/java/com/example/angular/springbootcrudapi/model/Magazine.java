package com.example.angular.springbootcrudapi.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Time;
import java.util.Calendar;


@NoArgsConstructor
@AllArgsConstructor
@Table
@Entity
@SequenceGenerator(name="seq", initialValue=1, allocationSize=1)
public class Magazine {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private int periodicite;
    private int numero;
    private int oeuvreId;
    private java.sql.Date date_parution=new java.sql.Date(Calendar.getInstance().getTime().getTime());


    public int getNumero() {
        return numero;
    }
    public void setNumero(int numero) {
        this.numero = numero;
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public int getPeriodicite() {
        return periodicite;
    }
    public void setPeriodicite(int periodicite) {
        this.periodicite = periodicite;
    }
    public Date getDate_parution() {
        return date_parution;
    }
    public void setDate_parution(Date date_parution) {
        this.date_parution = date_parution;
    }
    public int getOeuvreId() {
        return oeuvreId;
    }
    public void setOeuvreId(int oeuvre_id) {
        this.oeuvreId = oeuvre_id;
    }
}

package com.example.angular.springbootcrudapi.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Time;
import java.util.Calendar;


@NoArgsConstructor
@AllArgsConstructor
@Table
@Entity
@SequenceGenerator(name="seq", initialValue=1, allocationSize=1)
public class Reservation {
	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
	
	@Column(name = "fk_usager_id")
	private int fk_usager_id;
	
	@Column(name = "fk_oeuvre_id")
	private int fk_oeuvre_id;
	
	private java.sql.Date date_reservation=new java.sql.Date(Calendar.getInstance().getTime().getTime());
	
	public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public Date getDate() {
        return date_reservation;
    }

    public void setDate(Date date) {
        this.date_reservation = date;
    }
	public int getFk_usager_id() {
		return fk_usager_id;
	}
	public void setFk_usager_id(int fk_usager_id) {
		this.fk_usager_id = fk_usager_id;
	}
	public int getFk_oeuvre_id() {
		return fk_oeuvre_id;
	}
	public void setFk_oeuvre_id(int fk_oeuvre_id) {
		this.fk_oeuvre_id = fk_oeuvre_id;
	}

}

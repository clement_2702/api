package com.example.angular.springbootcrudapi.repository;

import com.example.angular.springbootcrudapi.model.Emprunt;
import org.springframework.context.support.ApplicationObjectSupport;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmpruntRepository extends JpaRepository<Emprunt,Integer> {
    @Query(
            value = "SELECT * FROM emprunt INNER JOIN usager ON (usager.id=usager_id) where CONCAT(usager.nom,' ', usager.prenom) LIKE ?1 OR CONCAT(usager.prenom, ' ', usager.nom) LIKE ?1",
            nativeQuery = true)
    List<Emprunt> getEmpruntByUsagerName(String str);

    @Query(
            value = "SELECT * FROM emprunt INNER JOIN usager ON (usager.id=usager_id) INNER JOIN exemplaire ON (exemplaire.id=exemplaire_id) INNER JOIN oeuvre ON (exemplaire.oeuvre_id=oeuvre.id) where (CONCAT(usager.nom,' ', usager.prenom) LIKE ?1 OR CONCAT(usager.prenom, ' ', usager.nom) LIKE ?1) AND oeuvre.titre LIKE ?2",
            nativeQuery = true)
    List<Emprunt> getEmpruntByUsagerNameAndOeuvreTitre(String str, String ste);

    @Query(
            value = "SELECT * FROM emprunt INNER JOIN exemplaire ON (exemplaire.id=emprunt.exemplaire_id) INNER JOIN oeuvre ON (oeuvre.id=exemplaire.oeuvre_id) where oeuvre.titre LIKE ?1",
            nativeQuery = true)
    List<Emprunt> getEmpruntByOeuvreTitre(String str);

    @Query(
            value = "SELECT * FROM emprunt where emprunt.usager_id=?1 LIMIT 1",
            nativeQuery = true)
    Emprunt getEmpruntByUsagerId(int i);

    @Query(
            value = "SELECT * FROM emprunt INNER JOIN exemplaire ON (exemplaire.id=emprunt.exemplaire_id) WHERE exemplaire.oeuvre_id=?1 LIMIT 1;",
            nativeQuery = true)
    Emprunt getEmpruntByOeuvreId(int i);
}
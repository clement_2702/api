package com.example.angular.springbootcrudapi.repository;

import com.example.angular.springbootcrudapi.model.Exemplaire;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ExemplaireRepository extends JpaRepository<Exemplaire,Integer> {
    List<Exemplaire> findByOeuvreId(int oeuvreId);

    @Query(
            value = "SELECT * FROM exemplaire where exemplaire.oeuvre_id LIKE ?1 AND etat='Bon état'",
            nativeQuery = true)
    List<Exemplaire> findByOeuvreIdDispo(int oeuvreId);


    @Query(
            value = "SELECT * FROM exemplaire where exemplaire.oeuvre_id=?1 LIMIT 1",
            nativeQuery = true)
    Exemplaire getExemplaireByOeuvreId(int oeuvreId);

    @Query(
            value = "SELECT * FROM exemplaire where exemplaire.id=?1 AND exemplaire.etat='Emprunté' LIMIT 1",
            nativeQuery = true)
    Exemplaire getOneExemplaireEmprunt(Integer exemplaireId);
}

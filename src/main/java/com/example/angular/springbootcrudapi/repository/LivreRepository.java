package com.example.angular.springbootcrudapi.repository;

import com.example.angular.springbootcrudapi.model.Exemplaire;
import com.example.angular.springbootcrudapi.model.Livre;
import com.example.angular.springbootcrudapi.model.Magazine;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface LivreRepository extends JpaRepository<Livre,Integer> {
    Optional<Livre> findByOeuvreId(Integer oeuvreId);

    @Query(
            value = "SELECT * FROM livre where livre.oeuvre_id=?1",
            nativeQuery = true)
    Livre getLivreByOeuvreId(int oeuvreId);
}

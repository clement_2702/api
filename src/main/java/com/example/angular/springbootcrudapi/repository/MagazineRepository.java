package com.example.angular.springbootcrudapi.repository;

import com.example.angular.springbootcrudapi.model.Exemplaire;
import com.example.angular.springbootcrudapi.model.Livre;
import com.example.angular.springbootcrudapi.model.Magazine;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface MagazineRepository extends JpaRepository<Magazine,Integer> {
    Optional<Magazine> findByOeuvreId(Integer oeuvreId);

    @Query(
            value = "SELECT * FROM magazine where magazine.oeuvre_id=?1",
            nativeQuery = true)
    Magazine getMagazineByOeuvreId(int oeuvreId);
}

package com.example.angular.springbootcrudapi.repository;

import com.example.angular.springbootcrudapi.model.Emprunt;
import com.example.angular.springbootcrudapi.model.Exemplaire;
import com.example.angular.springbootcrudapi.model.Oeuvre;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OeuvreRepository extends JpaRepository<Oeuvre,Integer> {
    @Query(
            value = "Select * from oeuvre INNER JOIN exemplaire ON (oeuvre.id=oeuvre_id) where exemplaire.id=?1",
            nativeQuery = true)
    Oeuvre getOeuvreByExemplaireId(int i);

}

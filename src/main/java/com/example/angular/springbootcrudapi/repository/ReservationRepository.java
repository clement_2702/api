package com.example.angular.springbootcrudapi.repository;

import java.util.List;
import java.util.Optional;

import com.example.angular.springbootcrudapi.model.Emprunt;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.angular.springbootcrudapi.model.Reservation;

@Repository
public interface ReservationRepository extends JpaRepository<Reservation,Integer> {
    @Query(
            value = "SELECT * FROM reservation where reservation.fk_usager_id LIKE ?1 AND reservation.fk_oeuvre_id LIKE ?2",
            nativeQuery = true)
    List <Reservation> findByUsagerIdAndOeuvreId(int usagerId, int oeuvreId);

    @Query(
            value = "SELECT * FROM reservation where reservation.fk_usager_id LIKE ?1",
            nativeQuery = true)
    List <Reservation> findByUsagerId(int usagerId);

    @Query(
            value = "SELECT * FROM reservation where reservation.fk_oeuvre_id LIKE ?1",
            nativeQuery = true)
    List <Reservation> findByOeuvreId(int oeuvreId);

}

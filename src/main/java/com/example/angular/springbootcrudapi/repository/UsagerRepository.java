package com.example.angular.springbootcrudapi.repository;

import com.example.angular.springbootcrudapi.model.Usager;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsagerRepository extends JpaRepository<Usager,Integer> {
}
